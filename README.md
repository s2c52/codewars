# 8kan 
[Remove String Spaces](https://www.codewars.com/kata/57eae20f5500ad98e50002c5)
### Código:
```
std::string no_space(std::string x)
{
      std::string result_str="";
    for(int c=0;c<x.size();c++){
	    if(x[c]!=' ')
	        {result_str+=x[c];
	        }
	    }
    return result_str;
}
```
### Pruebas unitarias
```
Describe(Basic_tests)
{
    It(Basic_test){
		Assert::That(no_space("8 j 8   mBliB8g  imjB8B8  jl  B"), Equals("8j8mBliB8gimjB8B8jlB"));
		Assert::That(no_space("8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd"), Equals("88Bifk8hB8BB8BBBB888chl8BhBfd"));
		Assert::That(no_space("8aaaaa dddd r     "), Equals("8aaaaaddddr"));
		Assert::That(no_space("jfBm  gk lf8hg  88lbe8 "), Equals("jfBmgklf8hg88lbe8")); 
		Assert::That(no_space("8j aam"), Equals("8jaam"));
    }
};
```

# 7 Kyu
[Printer Errors](https://www.codewars.com/kata/56541980fa08ab47a0000040)
### Código:
```
#include <iostream>
#include <algorithm>
class Printer
{
public:
    static std::string printerError(const std::string &s);
};
std::string Printer::printerError(const std::string &s){
    int errsum = 0;
    std::string str = s;
    std::string result ="";
    for(char& c:str){
        if(c>'m') {errsum+=1;}
    }
    result = result+std::to_string(errsum)+"/"+std::to_string(s.size());
    return result;
}
```
### Pruebas unitarias
```
void testequal(std::string ans, std::string sol) {
    Assert::That(ans, Equals(sol));
}
void dotest(std::string s, std::string expected)
{
    testequal(Printer::printerError(s), expected);
}
Describe(printerError_Tests)
{
    It(Fixed__Tests)
    {
        std::string s = "aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz";
        dotest(s, "3/56");
        s = "kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz";
        dotest(s, "6/60");
        s = "kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyzuuuuu";
        dotest(s, "11/65");
    }
};

```

# 6 kyu
[How many pages in a book?](https://www.codewars.com/kata/622de76d28bf330057cd6af8)

### Código:
```
function amountOfPages(summary){
  if(summary <= 9) return summary
  var count = 0;
  var res = 0;
  for(let i = 1; i<summary; i++){
    count+=i.toString().length
    if(count==summary) res = i
  }
  return res
}
```
### Pruebas unitarias
```
const chai = require("chai");
const assert = chai.assert;

describe("Solution", function() {
  it("should test for something", function() {
    // Test.assertEquals(1 + 1, 2);
    // assert.strictEqual(1 + 1, 2);
        assert.strictEqual(amountOfPages(5), 5)
        assert.strictEqual(amountOfPages(25), 17)
        assert.strictEqual(amountOfPages(1095), 401)        
        assert.strictEqual(amountOfPages(185), 97)
        assert.strictEqual(amountOfPages(660), 256)
  });
});

```


# 5 Kyu
[Prime Time](https://www.codewars.com/kata/521ef596c106a935c0000519)

### Código:
```
function prime(num) {
function isPrime(n) {
  if(n == 2) return true
     for (let i = 2; i < n; i++) {
        if (n % i == 0) return false
    }
  return true
}
  var r = []
  for(let i = 2; i <= num; i++){
    if(isPrime(i)) r.push(i)
  }
  return r
}
```
### Pruebas unitarias
```
describe("Basic tests", function(){
Test.assertDeepEquals(prime(0),[]);
Test.assertDeepEquals(prime(1),[]);
Test.assertDeepEquals(prime(2),[2]);
Test.assertDeepEquals(prime(23),[2,3,5,7,11,13,17,19,23]);
});
```

# 4Kyu
[Strip Comments](https://www.codewars.com/kata/51c8e37cee245da6b40000bd/train/python)

### Código:
```
def solution(string,markers):
    ans = []
    for line in string.split('\n'):
        for marker in markers:
            index = line.find(marker)
            if index != -1:
                line = line[:index]
        ans.append(line.strip())
    return '\n'.join(ans)
```
### Pruebas unitarias
```
# -*- coding: utf-8 -*-
test.assert_equals(solution("apples, pears # and bananas\ngrapes\nbananas !apples", ["#", "!"]), "apples, pears\ngrapes\nbananas")
test.assert_equals(solution("a #b\nc\nd $e f g", ["#", "$"]), "a\nc\nd")
```

# 3 Kyu
[Last Digit of a Huge Number](https://www.codewars.com/kata/last-digit-of-a-huge-number)
### Código:
```
#include <list>
#include <cmath>
using namespace std;

int last_digit(list<int> arr) {
  long result = 1;

  for (auto i = arr.rbegin(); i != arr.rend(); i++)
  {
    int base = (*i < 20) ? *i : *i % 20 + 20;
    int exponent = (result < 4) ? result : result % 4 + 4;

    result = std::pow(base,exponent);
  }
  return result % 10;
}
```
### Pruebas unitarias
```
#include <math.h>

Describe(Last_digit_of_a_huge_number) {
    It(Sample_test) {
        Assert::That(last_digit({}),         Equals(1));
        Assert::That(last_digit({0,0}),      Equals(1));
        Assert::That(last_digit({0,0,0}),    Equals(0));
        Assert::That(last_digit({1,2}),      Equals(1));
        Assert::That(last_digit({3,4,5}),    Equals(1));
        Assert::That(last_digit({4,3,6}),    Equals(4));
        Assert::That(last_digit({7,6,21}),   Equals(1));
        Assert::That(last_digit({12,30,21}), Equals(6));
        Assert::That(last_digit({2,2,2,0}),  Equals(4));
        Assert::That(last_digit({937640,767456,981242}),  Equals(0));
        Assert::That(last_digit({123232,694022,140249}),  Equals(6));
        Assert::That(last_digit({499942,898102,846073}),  Equals(6));
        
        int rand1 = rand()%100;
        int rand2 = rand()%10;
        Assert::That(last_digit({rand1}), Equals(rand1%10));
        Assert::That(last_digit({rand1,rand2}), Equals((int)pow(rand1%10,rand2)%10));
    }
};
```


# 2 Kyu

[Multi Line Task++: Hello World](https://www.codewars.com/kata/5935558a32fb828aad001213)
### Código:
```
f=

''

[

'\

t\

r\

i\

m'

][

'\

b\

i\

n\

d'

]`

H\

e\

l\

l\

o\

,\

\

w\

o\

r\

l\

d\

!`
```
### Pruebas unitarias
```
describe("Basic tests", function(){
  it("It should work for basic tests", function(){
    Test.assertEquals(f(),'Hello, world!');
  });
});

describe("Code length check", function(){
  it("Every line should have less than 3 characters", function(){
    const limit = 3;
    const lines = usercode.split('\n');
    Test.expect(Math.max(...lines.map(s=>s.length))<limit, `Some lines of your code exceeded the character limit`);
  });
  it("There should be less than 40 lines", function(){
    const limit = 40;
    const lines = usercode.split('\n');
    Test.expect(lines.length<limit, `Your code has ${lines.length} lines`);
  });
});
```

# 1 Kyu
No fui capas de encontrar algun ejercicicio de 1 kyu que pudiera resolver



# Conclusion
Mediante las pruebas planteadas sé ve la solucion a como resolver el problema, basicamente TDD.
